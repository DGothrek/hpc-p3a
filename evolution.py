"""Strategy to find the best parameters according to
   the input parameters and algorithm type.
   Uses an evolutionary algorithm."""

import numpy as np
import random as rd
import subprocess
import csv

generation_size = 30
evolution_steps = 8
keep_percentage = 0.3
mutation_rate = 0.1

parameters_list = {}
parameters_list['barrier_arities'] = [2, 4, 8, 16, 32]
parameters_list['broadcast_arity_maxs'] = [2, 4, 8, 16, 32]
parameters_list['broadcast_max_sizes'] = [1024, 2048, 4096, 8192, 16384]
parameters_list['allreduce_arity_maxs'] = [2, 4, 8, 16, 32]
parameters_list['allreduce_max_sizes'] = [1024, 2048, 4096, 8192, 16384]
parameters_list['collectives_init_hooks'] = ['sctk_collectives_init_opt_messages',
                                             'sctk_collectives_init_opt_noalloc_split_messages']
parameters_list['eager_limits'] = [2048, 4096, 8192, 16384]
parameters_list['buffered_limits'] = [4096, 8192, 16384, 32768, 65536, 131072]
parameters_list['qp_tx_depths'] = [2048, 4096, 8192, 16384]
parameters_list['qp_rx_depths'] = [0, 2048, 4096, 8192, 16384]
parameters_list['cq_depths'] = [8192, 16384, 32768, 65536, 131072]

fields = ['nodes', "barrier_arity",
          "broadcast_arity_max", "broadcast_max_size",
          "allreduce_arity_max", "allreduce_max_size",
          "collectives_init_hook", "eager_limit",
          "buffered_limit", "qp_tx_depth",
          "qp_rx_depth", "cq_depth",
          '4', '8', '16', '32', '64', '128', '256',
          '512', '1024', '2048', '4096', '8192',
          '16384', '32768', '65536',
          '131072', '262144', '524288']


def generate_config(barrier_arity=8,
                    broadcast_arity_max=32,
                    broadcast_max_size=1024,
                    allreduce_arity_max=8,
                    allreduce_max_size=4096,
                    collectives_init_hook="sctk_collectives_init_opt_noalloc_split_messages",
                    eager_limit=12288,
                    buffered_limit=262114,
                    qp_tx_depth=15000,
                    qp_rx_depth=0,
                    cq_depth=40000,
                    ):
    """Generates a configuration file ready to use for mpcrun."""

    params = {
        "barrier_arity": barrier_arity,
        "broadcast_arity_max": broadcast_arity_max,
        "broadcast_max_size": broadcast_max_size,
        "allreduce_arity_max": allreduce_arity_max,
        "allreduce_max_size": allreduce_max_size,
        "collectives_init_hook": collectives_init_hook,
        "eager_limit": eager_limit,
        "buffered_limit": buffered_limit,
        "qp_tx_depth": qp_tx_depth,
        "qp_rx_depth": qp_rx_depth,
        "cq_depth": cq_depth
    }
    res = ""
    with open('params.xml', 'r') as default_config:
        lines = default_config.readlines()
        for line in lines:
            found = False
            toremove = ""
            for param in params:
                if (param in line):
                    res += "<{}>{}</{}>\n".format(param, params[param], param)
                    found = True
                    toremove = param
            if not(found):
                res += line
            if toremove != "":
                params.pop(toremove)
    with open('config.xml', 'w') as f:
        f.write(res)


def generate_generation_parameters(results=None):
    """Generates new generation parameters according to
    the results of the previous executions

    Input:
        - results: Pandas DataFrame that contains the execution time that must be optimized
    Outputs:
        - parameters: a list of dicts that contains the parameters that should be used in the next generation"""

    def generate_random_generation():
        new_parameters_list = []
        for _ in range(generation_size):
            params = {}

            t = [0]*11

            t[0] = rd.randrange(0, len(parameters_list['barrier_arities']))
            t[1] = rd.randrange(
                0, len(parameters_list['broadcast_arity_maxs']))
            t[2] = rd.randrange(0, len(parameters_list['broadcast_max_sizes']))
            t[3] = rd.randrange(
                0, len(parameters_list['allreduce_arity_maxs']))
            t[4] = rd.randrange(0, len(parameters_list['allreduce_max_sizes']))
            t[5] = rd.randrange(
                0, len(parameters_list['collectives_init_hooks']))
            t[6] = rd.randrange(0, len(parameters_list['eager_limits']))
            t[7] = rd.randrange(0, len(parameters_list['buffered_limits']))
            t[8] = rd.randrange(0, len(parameters_list['qp_tx_depths']))
            t[9] = rd.randrange(0, len(parameters_list['qp_rx_depths']))
            t[10] = rd.randrange(0, len(parameters_list['cq_depths']))

            params['barrier_arity'] = parameters_list['barrier_arities'][t[0]]
            params['broadcast_arity_max'] = parameters_list['broadcast_arity_maxs'][t[1]]
            params['broadcast_max_size'] = parameters_list['broadcast_max_sizes'][t[2]]
            params['allreduce_arity_max'] = parameters_list['allreduce_arity_maxs'][t[3]]
            params['allreduce_max_size'] = parameters_list['allreduce_max_sizes'][t[4]]
            params['collectives_init_hook'] = parameters_list['collectives_init_hooks'][t[5]]
            params['eager_limit'] = parameters_list['eager_limits'][t[6]]
            params['buffered_limit'] = parameters_list['buffered_limits'][t[7]]
            params['qp_tx_depth'] = parameters_list['qp_tx_depths'][t[8]]
            params['qp_rx_depth'] = parameters_list['qp_rx_depths'][t[9]]
            params['cq_depth'] = parameters_list['cq_depths'][t[10]]

            new_parameters_list.append(params)
        return new_parameters_list

    if results == None:
        # For the first generation, generate a brand new random set of parameters
        return generate_random_generation()

    # Examine the results and keep the best ones, reproduce them 
    # and do some mutations to cover all the domain

    # Selection process
    # TODO Keep only keep_percentage of the most successful configurations
    # NB : This is a very straightforward implementation, we could choose a more 

    results_max = np.array([result['524288'] for result in results if result['524288']!=-1.])
    
    indices_sorted = np.argsort(results_max)
    indices_keep = indices_sorted[int(len(indices_sorted)*(1-keep_percentage)): len(indices_sorted)]

    selected_params = results[indices_keep]

    new_params = np.copy(selected_params)

    if len(selected_params)<2:
        # Too few correct executions to perform correctly
        return generate_random_generation()

    # Reproduction process
    while len(new_params)<generation_size:
        parent1, parent2 = rd.sample(selected_params,2)
        child = {}
        for key in parent1:
            

    # Random mutations
    # TBD

    return


def test_generation(parameters_list, n_generation):
    """Run the benchmark on this generation and return the execution evaluation."""
    result = []
    for params in parameters_list:
        nodes = 32
        N, p, n = nodes, nodes, 16*nodes
        generate_config(**params)
        command = 'mpcrun -N={} -p={} -n={} -c=16 --config=./config.xml --autokill=1200 --opt="-p sandy" ./osu-micro-benchmarks-5.5/mpi/collective/osu_allreduce'.format(
            N, p, n)

        out = subprocess.Popen(
            command, stdout=subprocess.PIPE, shell=True).stdout.readlines()

        print(out)
        lineres = []
        try:
            for partres in out[4:]:

                timeres = partres[:-1]
                timeres = timeres.split(' ')
                print(float(timeres[-1]))
                lineres.append(float(timeres[-1]))
        except:
            lineres = [-1.]*18
        params.update({"nodes": nodes,
                             "4": lineres[0],
                             "8": lineres[1],
                             "16": lineres[2],
                             "32": lineres[3],
                             "64": lineres[4],
                             "128": lineres[5],
                             "256": lineres[6],
                             "512": lineres[7],
                             "1024": lineres[8],
                             "2048": lineres[9],
                             "4096": lineres[10],
                             "8192": lineres[11],
                             "16384": lineres[12],
                             "32768": lineres[13],
                             "65536": lineres[14],
                             "131072": lineres[15],
                             "262144": lineres[16],
                             "524288": lineres[17]})
        result.append(params)

    return result


def optimal_mpc_evolution():
    """Runs the generic evolution from a random distribution, outputs a CSV log"""
    parameters_list = None
    results = None
    for n_generation in range(evolution_steps):
        parameters_list = generate_generation_parameters(results)
        results = test_generation(parameters_list, n_generation)
        path_out = 'output_gen_{}.csv'.format(n_generation)
        with open(path_out, 'w') as out_file:
            csvwriter = csv.DictWriter(out_file, fields, delimiter='\t')
            csvwriter.writeheader()
            for test in results:
                csvwriter.writerow(test)
    return


optimal_mpc_evolution()
