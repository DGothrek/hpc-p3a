# TODO for efficient P3A

## Parameters

inter_thread_comm parameters

	arity : 2 ->32
	broadcast : 2 ->32
	broadcast_size : 1024 -> 16k
	reduce : idem

infiniband 
	limites à la baisse (par puissance de 2)
	eager : 2 au dessus, 2 en dessous
	depth : 3/4 valeurs
opa (intel) ignore

network : eager_limit -> cq_depth

path for the functions initializing network : mpcframework/MPC_Message_Passing/sctk_inter_thread_comm


## OSU

Multi_lat (pt2pt)
Barrier (collectives)
All_reduce (collectives)

-n=16xX -N=X -p=X -c=16 (nombre de coeurs maximisé) 8-16-32

mpcrun --config=XMLFILE

Paramètres XML - juste ceux nécessaires.