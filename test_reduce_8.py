import csv
import os
import random as rd
import subprocess
import xml.etree.cElementTree as ET


def generate_one_config(barrier_arity=8,
                        broadcast_arity_max=32,
                        broadcast_max_size=1024,
                        allreduce_arity_max=8,
                        allreduce_max_size=4096,
                        collectives_init_hook="sctk_collectives_init_opt_noalloc_split_messages",
                        eager_limit=12288,
                        buffered_limit=262114,
                        qp_tx_depth=15000,
                        qp_rx_depth=0,
                        cq_depth=40000,
                        ):
    root = ET.Element("mpc")

    root.set('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance")
    root.set('xsi:noNamespaceSchemaLocation', "mpc-config.xsd")
    profiles = ET.SubElement(root, "profiles")
    profile = ET.SubElement(profiles, "profile")
    ET.SubElement(profile, "name").text = 'default'
    modules = ET.SubElement(profile, "modules")

    # Inter Thread Communication
    inter_thread_comm = ET.SubElement(modules, "inter_thread_comm")
    ET.SubElement(inter_thread_comm, "barrier_arity").text = str(barrier_arity)
    ET.SubElement(inter_thread_comm, "broadcast_arity_max").text = str(
        broadcast_arity_max)
    ET.SubElement(inter_thread_comm, "broadcast_max_size").text = str(
        broadcast_max_size)
    ET.SubElement(inter_thread_comm, "allreduce_arity_max").text = str(
        allreduce_arity_max)
    ET.SubElement(inter_thread_comm, "allreduce_max_size").text = str(
        allreduce_max_size)
    ET.SubElement(inter_thread_comm,
                  "collectives_init_hook").text = collectives_init_hook

    # Infiniband Configuration

    networks = ET.SubElement(profile, "networks")
    configs = ET.SubElement(networks, "configs")
    config = ET.SubElement(configs, "config")
    ET.SubElement(config, "name").text = "ib_config_mpi"
    driver = ET.SubElement(config, "driver")
    infiniband = ET.SubElement(driver, "infiniband")
    ET.SubElement(infiniband, "eager_limit").text = str(eager_limit)
    ET.SubElement(infiniband, "buffered_limit").text = str(buffered_limit)
    ET.SubElement(infiniband, "qp_tx_depth").text = str(qp_tx_depth)
    ET.SubElement(infiniband, "qp_rx_depth").text = str(qp_rx_depth)
    ET.SubElement(infiniband, "cq_depth").text = str(cq_depth)
    # config = ET.SubElement(configs, "config")
    # ET.SubElement(config, "name").text = "shm_config_mpi"

    tree = ET.ElementTree(root)
    tree.write("config.xml")
    return


def generate_one_complete_config(barrier_arity=8,
                                 broadcast_arity_max=32,
                                 broadcast_max_size=1024,
                                 allreduce_arity_max=8,
                                 allreduce_max_size=4096,
                                 collectives_init_hook="sctk_collectives_init_opt_noalloc_split_messages",
                                 eager_limit=12288,
                                 buffered_limit=262114,
                                 qp_tx_depth=15000,
                                 qp_rx_depth=0,
                                 cq_depth=40000,
                                 ):
    params = {
        "barrier_arity": barrier_arity,
        "broadcast_arity_max": broadcast_arity_max,
        "broadcast_max_size": broadcast_max_size,
        "allreduce_arity_max": allreduce_arity_max,
        "allreduce_max_size": allreduce_max_size,
        "collectives_init_hook": collectives_init_hook,
        "eager_limit": eager_limit,
        "buffered_limit": buffered_limit,
        "qp_tx_depth": qp_tx_depth,
        "qp_rx_depth": qp_rx_depth,
        "cq_depth": cq_depth
    }
    res = ""
    with open('params.xml', 'r') as default_config:
        lines = default_config.readlines()
        for line in lines:
            found = False
            toremove = ""
            for param in params:
                if (param in line):
                    res += "<{}>{}</{}>\n".format(param, params[param], param)
                    found = True
                    toremove = param
            if not(found):
                res += line
            if toremove != "":
                params.pop(toremove)
    with open('config.xml', 'w') as f:
        f.write(res)


def generate_config(nconfigs):
    # Parameters we want to experiment with:
    barrier_arities = [2, 4, 8, 16, 32]
    broadcast_arity_maxs = [2, 4, 8, 16, 32]
    broadcast_max_sizes = [1024, 2048, 4096, 8192, 16384]
    allreduce_arity_maxs = [2, 4, 8, 16, 32]
    allreduce_max_sizes = [1024, 2048, 4096, 8192, 16384]
    collectives_init_hooks = ['sctk_collectives_init_opt_messages',
                              'sctk_collectives_init_opt_noalloc_split_messages']
    eager_limits = [2048, 4096, 8192, 16384]
    buffered_limits = [4096, 8192, 16384, 32768, 65536, 131072]
    qp_tx_depths = [2048, 4096, 8192, 16384]
    qp_rx_depths = [0, 2048, 4096, 8192, 16384]
    cq_depths = [8192, 16384, 32768, 65536, 131072]

    # barrier_arities = [8]
    # broadcast_arity_maxs = [32]
    # broadcast_max_sizes = [1024]
    # allreduce_arity_maxs = [8]
    # allreduce_max_sizes = [4096]

    # collectives_init_hooks = [
    #     'sctk_collectives_init_opt_noalloc_split_messages','sctk_collectives_init_hetero_messages']
    # eager_limits = [12288]
    # buffered_limits = [262114]
    # qp_tx_depths = [15000]
    # qp_rx_depths = [0]
    # cq_depths = [40000]

    fields = ['x', 'ba', 'bam', 'bms', 'aam', 'ams',
              'cih', 'el', 'bl', 'qtd', 'qrd', 'cd',
              '4', '8', '16', '32', '64', '128', '256',
              '512', '1024', '2048', '4096', '8192', '16384', '32768', '65536',
              '131072', '262144', '524288']
    X = [8]
    # X = [1]
    for x in X:
        with open('results_allreduce_{}.csv'.format(x), 'a') as out_file:
            csvwriter = csv.DictWriter(out_file, fields, delimiter='\t')
            # csvwriter.writeheader()
            t = [0]*11
            N, p, n = x, x, 16*x
            command = 'mpcrun -N={} -p={} -n={} -c=16 --config=./config.xml --autokill=600 --opt="-p sandy" ./osu-micro-benchmarks-5.5/mpi/collective/osu_allreduce'.format(
                N, p, n)
            for _ in range(nconfigs):
                t[0] = rd.randrange(0, len(barrier_arities))
                t[1] = rd.randrange(0, len(broadcast_arity_maxs))
                t[2] = rd.randrange(0, len(broadcast_max_sizes))
                t[3] = rd.randrange(0, len(allreduce_arity_maxs))
                t[4] = rd.randrange(0, len(allreduce_max_sizes))
                t[5] = rd.randrange(0, len(collectives_init_hooks))
                t[6] = rd.randrange(0, len(eager_limits))
                t[7] = rd.randrange(0, len(buffered_limits))
                t[8] = rd.randrange(0, len(qp_tx_depths))
                t[9] = rd.randrange(0, len(qp_rx_depths))
                t[10] = rd.randrange(0, len(cq_depths))

                generate_one_complete_config(barrier_arity=barrier_arities[t[0]],
                                             broadcast_arity_max=broadcast_arity_maxs[t[1]],
                                             broadcast_max_size=broadcast_max_sizes[t[2]],
                                             allreduce_arity_max=allreduce_arity_maxs[t[3]],
                                             allreduce_max_size=allreduce_max_sizes[t[4]],
                                             collectives_init_hook=collectives_init_hooks[t[5]],
                                             eager_limit=eager_limits[t[6]],
                                             buffered_limit=buffered_limits[t[7]],
                                             qp_tx_depth=qp_tx_depths[t[8]],
                                             qp_rx_depth=qp_rx_depths[t[9]],
                                             cq_depth=cq_depths[t[10]])
                out = subprocess.Popen(command,
                                       stdout=subprocess.PIPE,
                                       shell=True).stdout.readlines()
                # os.system('rm ./config.xml')
                print(out)
                lineres = []
                try:
                    for partres in out[4:]:

                        timeres = partres[:-1]
                        timeres = timeres.split(' ')
                        print(float(timeres[-1]))
                        lineres.append(float(timeres[-1]))
                        # time = float(out[4][:-1])  # Remove last char
                except:
                    lineres = [-1.]*18
                row = {"x": x,
                       "ba": barrier_arities[t[0]],
                       "bam": broadcast_arity_maxs[t[1]],
                       "bms": broadcast_max_sizes[t[2]],
                       "aam": allreduce_arity_maxs[t[3]],
                       "ams": allreduce_max_sizes[t[4]],
                       "cih": collectives_init_hooks[t[5]],
                       "el": eager_limits[t[6]],
                       "bl": buffered_limits[t[7]],
                       "qtd": qp_tx_depths[t[8]],
                       "qrd": qp_rx_depths[t[9]],
                       "cd": cq_depths[t[10]],
                       "4": lineres[0],
                       "8": lineres[1],
                       "16": lineres[2],
                       "32": lineres[3],
                       "64": lineres[4],
                       "128": lineres[5],
                       "256": lineres[6],
                       "512": lineres[7],
                       "1024": lineres[8],
                       "2048": lineres[9],
                       "4096": lineres[10],
                       "8192": lineres[11],
                       "16384": lineres[12],
                       "32768": lineres[13],
                       "65536": lineres[14],
                       "131072": lineres[15],
                       "262144": lineres[16],
                       "524288": lineres[17]
                       }
                csvwriter.writerow(row)
    return


# generate_one_complete_config()
generate_config(30)
